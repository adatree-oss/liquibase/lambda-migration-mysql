package au.com.adatree.liquibase

data class CfnResponse(val Status: String,
                       val PhysicalResourceId: String,
                       val StackId: String,
                       val RequestId: String,
                       val LogicalResourceId: String,
                       val Reason: String) {
}
