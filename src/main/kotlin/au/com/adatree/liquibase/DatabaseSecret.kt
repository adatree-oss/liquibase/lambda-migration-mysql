package au.com.adatree.liquibase

data class DatabaseSecret(
		val username: String,
		val password: String,
		val engine: String,
		val host: String,
		val port: Int
) {
	companion object {
		val JDBC_URL_TEMPLATE = "jdbc:%s://%s:%s/%s"
	}

	fun jdbcUrl(schema: String): String {
		return String.format(JDBC_URL_TEMPLATE, engine, host, port, schema)
	}
}
