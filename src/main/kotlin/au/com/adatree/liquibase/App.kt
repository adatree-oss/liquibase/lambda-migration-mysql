package au.com.adatree.liquibase

import CfnLiquibaseMigrationEvent
import CfnLiquibaseMigrationEvent.Companion.RESOURCE_PROPS_DB_SECRET_ID
import CfnLiquibaseMigrationEvent.Companion.RESOURCE_PROPS_LIQUIBASE_CHANGE_LOG_FILE
import CfnLiquibaseMigrationEvent.Companion.RESOURCE_PROPS_SERVICE_NAME
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.LambdaLogger
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import liquibase.Contexts
import liquibase.Liquibase
import liquibase.database.DatabaseFactory
import liquibase.database.jvm.JdbcConnection
import liquibase.exception.LiquibaseException
import liquibase.resource.ClassLoaderResourceAccessor
import software.amazon.awssdk.auth.credentials.DefaultCredentialsProvider
import software.amazon.awssdk.auth.signer.Aws4Signer
import software.amazon.awssdk.auth.signer.params.Aws4PresignerParams
import software.amazon.awssdk.http.SdkHttpFullRequest
import software.amazon.awssdk.http.SdkHttpMethod
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient.builder
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueRequest
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueResponse
import java.io.IOException
import java.net.URI
import java.net.URISyntaxException
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpRequest.BodyPublishers
import java.net.http.HttpResponse
import java.sql.DriverManager
import java.sql.SQLException
import java.time.Instant

class App : RequestHandler<Map<String, Any>, String> {
	enum class Status {
		SUCCESS, FAILED
	}

	override fun handleRequest(input: Map<String, Any>, context: Context): String {
		val logger: LambdaLogger = context.logger
		logger.log("[INFO] Received Cfn request for DB Migration $input\n")
		val event = CfnLiquibaseMigrationEvent(input)
		logger.log("[INFO] Schema migration event received: $event\n")
		if (CfnLiquibaseMigrationEvent.REQUEST_TYPE_DELETE.equals(event.requestType, true)) {
			logger.log("[INFO] RequestType ${event.requestType} -> Exiting NOW\n")
			return sendCFNResponseToBuildProcess(event, Status.SUCCESS, context)
		} else if ((event.resourceProperties[CfnLiquibaseMigrationEvent.RESOURCE_PROPS_FEATURE_TOGGLE].toString().toBoolean()).not()) {
			logger.log("[INFO] Toggle is off, Exiting NOW ${event.requestType}\n")
			return sendCFNResponseToBuildProcess(event, Status.SUCCESS, context)
		}
		val secretId = event.resourceProperties[RESOURCE_PROPS_DB_SECRET_ID] as String
		val serviceName = event.resourceProperties[RESOURCE_PROPS_SERVICE_NAME] as String
		val changeLogFile = event.resourceProperties[RESOURCE_PROPS_LIQUIBASE_CHANGE_LOG_FILE] as String
		val schemaName = serviceName + "db"
		val migUserName = serviceName + "mig"

		val databaseSecret = getDatabaseUrl(secretId, context)

		return if (databaseSecret != null) {
			logger.log("[INFO] DatabaseSecret fetched, requestType: ${event.requestType}\n")
			logger.log("[INFO] Connecting to DB using secretId: $secretId\n")
			try {
				val authToken = getAuthToken(databaseSecret.host,
					databaseSecret.port,
					migUserName,
					context)

				DriverManager.getConnection(
					databaseSecret.jdbcUrl(schemaName),
					migUserName,
					authToken).use { con ->
					logger.log("[INFO] Connected to DB!\n")
					val dataBase =
						DatabaseFactory.getInstance().findCorrectDatabaseImplementation(JdbcConnection(con))

					Liquibase(changeLogFile, ClassLoaderResourceAccessor(), dataBase).update(Contexts())
				}
			} catch (e: SQLException) {
				logger.log("[ERROR] Exception: ${e.message}")
				return sendCFNResponseToBuildProcess(event, Status.FAILED, context)
			} catch (e: LiquibaseException) {
				logger.log("[ERROR] Exception: ${e.message}")
				return sendCFNResponseToBuildProcess(event, Status.FAILED, context)
			}
			logger.log("[INFO] Evolved database $schemaName using secretId: $secretId\n")
			sendCFNResponseToBuildProcess(event, Status.SUCCESS, context)
		} else {
			logger.log("[ERROR] Unable to get DB Secret\n")
			sendCFNResponseToBuildProcess(event, Status.FAILED, context)
		}
	}

	fun sendCFNResponseToBuildProcess(event: CfnLiquibaseMigrationEvent, status: Status, context: Context): String {
		val logger: LambdaLogger = context.logger
		val responseURL: String = event.responseURL
		try {
			val cfnResp = GSON.toJson(
				buildCfnResponse(event, status, context.logStreamName, context.logGroupName))
			val request = HttpRequest.newBuilder(URI(responseURL)).PUT(BodyPublishers.ofString(cfnResp)).build()
			val response = HttpClient.newBuilder().build().send(request, HttpResponse.BodyHandlers.ofString())
			logger.log("[INFO] Response sent to responseURL, responseCode ${response.statusCode()}")
		} catch (e: IOException) {
			logger.log("[ERROR] Failed to send response to responseURL $responseURL \nStackTrace ${e.message}\n")
		} catch (e: URISyntaxException) {
			logger.log("[ERROR] Failed to send response to responseURL $responseURL \nStackTrace ${e.message}\n")
		} catch (e: InterruptedException) {
			logger.log("[ERROR] Failed to send response to responseURL $responseURL \nStackTrace ${e.message}\n")
		}
		return status.toString()
	}

	fun buildCfnResponse(event: CfnLiquibaseMigrationEvent, status: Status, logStreamName: String,
						 logGroupName: String): CfnResponse {
		return CfnResponse(
			status.name,
			logStreamName,
			event.stackId,
			event.requestId,
			event.logicalResourceId,
			"Details in CloudWatch Log Stream $logStreamName Group $logGroupName")
	}

	fun getDatabaseUrl(secretId: String, context: Context): DatabaseSecret? {
		val logger: LambdaLogger = context.getLogger()
		return try {
			logger.log("[INFO] Fetching secretId: $secretId\n")
			val secretValueRequest = GetSecretValueRequest.builder().secretId(secretId).build()
			val secretValueResult: GetSecretValueResponse = secretsClient.getSecretValue(secretValueRequest)
			val secretString = secretValueResult.secretString()
			GSON.fromJson(secretString, DatabaseSecret::class.java);
		} catch (e: Throwable) {
			logger.log("[ERROR] Failed to extract DB Secret: ${e.message}\n")
			e.printStackTrace()
			null
		}
	}

	/**
	 * Create an authorization token used to connect to a database that uses RDS IAM authentication.
	 */
	fun getAuthToken(hostname: String, port: Int, username: String, context: Context): String {
		context.logger.log("[INFO] Getting Auth Token $hostname $port $username\n")

		val resolveCredentials = DefaultCredentialsProvider.create().resolveCredentials()

		val params = Aws4PresignerParams
			.builder()
			.expirationTime(Instant.now().plusSeconds(EXPIRATION_FIFTEEN_MINUTES.toLong()))
			.awsCredentials(resolveCredentials)
			.signingName(SERVICE_NAME)
			.signingRegion(Region.AP_SOUTHEAST_2)
			.build()

		val request = SdkHttpFullRequest
			.builder()
			.encodedPath("/")
			.host(hostname)
			.port(port)
			.protocol("http") // Will be stripped off; but we need to satisfy SdkHttpFullRequest
			.method(SdkHttpMethod.GET)
			.appendRawQueryParameter("Action", "connect")
			.appendRawQueryParameter("DBUser", username)
			.build()

		return Aws4Signer.create().presign(request, params).uri.toString().substringAfter("http://")
	}

	companion object {
		val secretsClient: SecretsManagerClient = builder().region(Region.AP_SOUTHEAST_2).build()
		val GSON: Gson = GsonBuilder().setPrettyPrinting().create();
		const val SERVICE_NAME = "rds-db"
		const val EXPIRATION_FIFTEEN_MINUTES = 15 * 60
	}
}
