data class CfnLiquibaseMigrationEvent(val requestType: String,
                                      val serviceToken: String,
                                      val responseURL: String,
                                      val stackId: String,
                                      val requestId: String,
                                      val logicalResourceId: String,
                                      val resourceType: String,
                                      val resourceProperties: Map<String, Any>) {
	constructor(eventParamsMap: Map<String, Any>) :
			this(eventParamsMap[PARAM_REQUEST_TYPE].toString(),
					eventParamsMap[PARAM_SERVICE_TOKEN].toString(),
					eventParamsMap[PARAM_RESPONSE_URL].toString(),
					eventParamsMap[PARAM_STACK_ID].toString(),
					eventParamsMap[PARAM_REQUEST_ID].toString(),
					eventParamsMap[PARAM_LOGICAL_RESOURCE_ID].toString(),
					eventParamsMap[PARAM_RESOURCE_TYPE].toString(),
					eventParamsMap[PARAM_RESOURCE_PROPERTIES] as Map<String, Any>)

	companion object {
		const val PARAM_REQUEST_TYPE = "RequestType"
		const val PARAM_SERVICE_TOKEN = "ServiceToken"
		const val PARAM_RESPONSE_URL = "ResponseURL"
		const val PARAM_STACK_ID = "StackId"
		const val PARAM_REQUEST_ID = "RequestId"
		const val PARAM_LOGICAL_RESOURCE_ID = "LogicalResourceId"
		const val PARAM_RESOURCE_TYPE = "ResourceType"
		const val PARAM_RESOURCE_PROPERTIES = "ResourceProperties"
		const val REQUEST_TYPE_DELETE = "Delete"
		const val RESOURCE_PROPS_DB_SECRET_ID = "secretId"
		const val RESOURCE_PROPS_SERVICE_NAME = "serviceName"
		const val RESOURCE_PROPS_LIQUIBASE_CHANGE_LOG_FILE = "changeLogFile"
		const val RESOURCE_PROPS_FEATURE_TOGGLE = "enabled"
	}
}
