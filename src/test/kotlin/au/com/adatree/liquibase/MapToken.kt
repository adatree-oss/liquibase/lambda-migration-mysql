package au.com.adatree.liquibase

import com.google.gson.reflect.TypeToken

class MapToken : TypeToken<Map<String, Any>>()
