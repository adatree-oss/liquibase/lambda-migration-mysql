package au.com.adatree.liquibase

import com.google.gson.Gson
import org.junit.jupiter.api.Test
import java.util.concurrent.ThreadLocalRandom
import kotlin.test.assertEquals

class DatabaseSecretTest {
	@Test
	fun `parse database secret from json string`() {
		val secret: DatabaseSecret = loadSampleDatabaseSecret()
		assertEquals("blabla", secret.username)
		assertEquals("password1", secret.password)
		assertEquals("mysql", secret.engine)
		assertEquals("liquibase-lambda-mysql.ap-southeast-2.rds.amazonaws.com", secret.host)
		assertEquals(3306, secret.port)
	}

	@Test
	fun `generate jdbc url from secret`() {
		val secret: DatabaseSecret = loadSampleDatabaseSecret()
		val randomSchemaName = ThreadLocalRandom.current().nextInt().toString()
		val jdbcUrl = secret.jdbcUrl(randomSchemaName)
		assertEquals("jdbc:mysql://liquibase-lambda-mysql.ap-southeast-2.rds.amazonaws.com:3306/$randomSchemaName", jdbcUrl)
	}

	private fun loadSampleDatabaseSecret(): DatabaseSecret {
		val secretSample = TestUtils().loadJsonFile("src/test/resources/secretsample.json")
		return Gson().fromJson(secretSample, DatabaseSecret::class.java)
	}
}
