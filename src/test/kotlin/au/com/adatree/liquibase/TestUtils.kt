package au.com.adatree.liquibase

import java.io.IOException
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths
import java.util.concurrent.ThreadLocalRandom

class TestUtils {
	fun loadJsonFile(path: String): String {
		val stringBuilder = StringBuilder()
		try {
			Files.lines(Paths.get(path), StandardCharsets.UTF_8).use { stream -> stream.forEach { s: String -> stringBuilder.append(s) } }
		} catch (e: IOException) {
			e.printStackTrace()
		}
		return stringBuilder.toString()
	}

	fun randomString(): String = ThreadLocalRandom.current().nextInt().toString(16)
}
