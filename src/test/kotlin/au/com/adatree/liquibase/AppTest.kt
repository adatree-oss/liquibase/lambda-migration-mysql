package au.com.adatree.liquibase

import CfnLiquibaseMigrationEvent
import CfnLiquibaseMigrationEvent.Companion.RESOURCE_PROPS_DB_SECRET_ID
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.LambdaLogger
import com.google.gson.Gson
import io.mockk.spyk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.BDDMockito
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import java.util.concurrent.ThreadLocalRandom

@ExtendWith(MockitoExtension::class)
class AppTest {
	//TODO unify the usage of Mockito and Mockk
	var mockContext = Mockito.mock(Context::class.java)
	var mockLambdaLogger = Mockito.mock(LambdaLogger::class.java)

	@BeforeEach
	fun setup() {
		BDDMockito.given(mockContext.logger).willReturn(mockLambdaLogger)
		BDDMockito.given(mockContext.logGroupName).willReturn(TestUtils().randomString())
		BDDMockito.given(mockContext.logStreamName).willReturn(TestUtils().randomString())
	}

	@Test
	fun `build CloudFormation Response`() {
		val cfnEvent: Map<String, Any> = Gson().fromJson(TestUtils().loadJsonFile("src/test/resources/event.json"), MapToken().type)
		val event = CfnLiquibaseMigrationEvent(cfnEvent)
		val status: App.Status = App.Status.values().get(ThreadLocalRandom.current().nextInt(1))
		val logStreamName = MethodOrderer.Random().toString()
		val logGroupName = MethodOrderer.Random().toString()
		val (Status, PhysicalResourceId, StackId, RequestId, LogicalResourceId, Reason) = App().buildCfnResponse(event, status, logStreamName, logGroupName)
		assertEquals(event.logicalResourceId, LogicalResourceId)
		assertEquals(event.stackId, StackId)
		assertEquals(event.requestId, RequestId)
		assertEquals(status.name, Status)
		assertEquals(logStreamName, PhysicalResourceId)
		assertEquals("Details in CloudWatch Log Stream $logStreamName Group $logGroupName", Reason)
	}

	@Test
	fun `skip execution when toggle flag is off`() {
		val eventString: String = TestUtils().loadJsonFile("src/test/resources/event_disabled.json")
		val cfnEvent: Map<String, Any> = Gson().fromJson<Map<String, Any>>(eventString, MapToken().type)
		val app: App = spyk(App())
		app.handleRequest(cfnEvent, mockContext)
		verify { app.sendCFNResponseToBuildProcess(CfnLiquibaseMigrationEvent(cfnEvent), App.Status.SUCCESS, mockContext) }
		verify(exactly = 0) { app.getDatabaseUrl(any(), any()) }
	}

	@Test
	fun `exit straightaway when request Type is delete`() {
		val eventString: String = TestUtils().loadJsonFile("src/test/resources/event_requesttype_delete.json")
		val cfnEvent: Map<String, Any> = Gson().fromJson<Map<String, Any>>(eventString, MapToken().type)
		BDDMockito.given(mockContext.logger).willReturn(mockLambdaLogger)
		val app: App = spyk(App())
		app.handleRequest(cfnEvent, mockContext)
		verify { app.sendCFNResponseToBuildProcess(CfnLiquibaseMigrationEvent(cfnEvent), App.Status.SUCCESS, mockContext) }
		verify(exactly = 0) { app.getDatabaseUrl(any(), any()) }
	}

	@Test
	fun `failed to get database secret`() {
		val eventString: String = TestUtils().loadJsonFile("src/test/resources/event.json")
		val cfnEvent: Map<String, Any> = Gson().fromJson<Map<String, Any>>(eventString, MapToken().type)
		BDDMockito.given(mockContext.logger).willReturn(mockLambdaLogger)
		val app: App = spyk(App())
		app.handleRequest(cfnEvent, mockContext)
		verify { app.sendCFNResponseToBuildProcess(CfnLiquibaseMigrationEvent(cfnEvent), App.Status.FAILED, mockContext) }
		verify { app.getDatabaseUrl((cfnEvent[CfnLiquibaseMigrationEvent.PARAM_RESOURCE_PROPERTIES] as Map<String, String>)[RESOURCE_PROPS_DB_SECRET_ID].toString(), mockContext) }
	}
}
