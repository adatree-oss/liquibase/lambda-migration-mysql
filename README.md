## Functionality

* This is a library that help manages database schema migration with AWS Lambda function without the risk of deadlock
* CloudFormation CustomResource to invoke Lambda function when the containing product version is deployed to a tenant account

## Runtime
When artifact of lambda module changes or definition of the invoker changes, this function will be called by CloudFormation.

When the execution ends, this function will send either SUCCESS or FAILED response back to CloudFormation.

note: This operation is idempotent.

### Runtime configuration sample
Defined in [service-catalogue-release](https://gitlab.com/adatree-source/infra/platform/service-catalogue-release/${service})
```yaml
# Lambda function, only need define once
  LiquibaseMigrationFunction:
    Type: AWS::Serverless::Function
    FunctionName: service1-liquibase-db-migration
    Properties:
      CodeUri:
        Bucket: !Ref LiquibaseLambdaBucket
        Key: !Ref LiquibaseLambdaKey
      Handler: au.com.adatree.rdbmigration.RDSManager::handleRequest
      Runtime: java11
      Description: Java function
      MemorySize: 512
      Timeout: 60
      Tracing: Active
      EventInvokeConfig:
        MaximumRetryAttempts: 0
      Role: !GetAtt LiquibaseExecutionRole.Arn
      VpcConfig:
        SecurityGroupIds:
          - !Ref  'FargateContainerSecurityGroup'
        SubnetIds:
          - SubnetOne
          - SubnetTwo
  LiquibaseInvoker:
    Type: AWS::CloudFormation::CustomResource
    Version: '1.0'
    Properties:
      ServiceToken: !GetAtt LiquibaseMigrationFunction.Arn
      secretId: !Ref SecretsManagerDatabaseSecretName
      changeLogFile: db.changelog/liquibase-changeLog.xml
      enabled: true
#      force invocation on LambdaFunction update
      triggerOnFunctionUpdate: !Ref LiquibaseLambdaKey
  LiquibaseExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Join ['-', [!Ref ServiceName, "LiquibaseExecutionRole", !If [IsProdCondition, "prod", "non-prod"]]]
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AWSXrayWriteOnlyAccess
        - arn:aws:iam::aws:policy/AWSLambdaReadOnlyAccess
        - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
        - arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: [lambda.amazonaws.com]
            Action: ['sts:AssumeRole']
      Policies:
        - PolicyName: LiquibaseExecutionPolicy
          PolicyDocument:
            Statement:
              - Effect: Allow
                Action:
                  # Allow the Lambda function to obtain db secret for schema migration
                  - 'secretsmanager:GetSecretValue'
                  # allow iam auth to rds
                  - 'rds-db:*'
                Resource: '*'
```

### TODO
* Converts this into multiple-module project and provide a service module that bundle this library into a Lambda function and deploy it in AWS
* [Code quality plugins](https://github.com/gradle/kotlin-dsl-samples/blob/master/samples/code-quality/build.gradle.kts)
* Mockk
* Pipeline: configure access token and then change master branch build to: 1. `./gradle clean build` 2. `./gradlew release` 3. `./gradlew publish`