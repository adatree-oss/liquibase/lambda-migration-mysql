import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    `java-library`
    kotlin("jvm") version "1.3.41"
    `maven-publish`
    signing
}

group = "au.com.adatree"
version = "1.0.0"

java {
    withJavadocJar()
    withSourcesJar()
}

repositories {
    jcenter()
    mavenCentral()
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            artifactId = "liquibase-lambda-mysql"
            from(components["java"])
            versionMapping {
                usage("java-api") {
                    fromResolutionOf("runtimeClasspath")
                }
                usage("java-runtime") {
                    fromResolutionResult()
                }
            }
            pom {
                name.set("Liquibase Migration Lambda function for Mysql")
                description.set("Streamline database migration using Liquibase and AWS Lambda.")
                url.set("https://gitlab.com/adatree-oss/liquibase/lambda-migration-mysql")
                licenses {
                    license {
                        name.set("MIT License")
                        url.set("https://gitlab.com/adatree-oss/liquibase/lambda-migration-mysql/-/blob/master/LICENSE")
                    }
                }
                developers {
                    developer {
                        id.set("arthur")
                        name.set("Arthur Zhang")
                        email.set("arthur@adatree.com.au")
                    }
                }
                scm {
                    connection.set("scm:git:git@gitlab.com:adatree-oss/liquibase/lambda-migration-mysql.git")
                    developerConnection.set("scm:git:git@gitlab.com:adatree-oss/liquibase/lambda-migration-mysql.git")
                    url.set("https://gitlab.com/adatree-oss/liquibase/lambda-migration-mysql")
                }
            }
        }
    }
    repositories {
        maven {
            name = "ossrh"
            val releasesRepoUrl = "https://oss.sonatype.org/service/local/staging/deploy/maven2/"
            val snapshotsRepoUrl = "https://oss.sonatype.org/content/repositories/snapshots/"
            val isSnapshot = version.toString().endsWith("SNAPSHOT")
            url = uri(if (isSnapshot) snapshotsRepoUrl else releasesRepoUrl)
            credentials {
                username = System.getenv("OSSRH_USERNAME")
                password = System.getenv("OSSRH_PASSWORD")
            }
        }
    }
}

signing {
    useInMemoryPgpKeys(System.getenv("SIGNING_KEY"), System.getenv("SIGNING_PASSWORD"))
    sign(publishing.publications["mavenJava"])
}

tasks.javadoc {
    if (JavaVersion.current().isJava9Compatible) {
        (options as StandardJavadocDocletOptions).addBooleanOption("html5", true)
    }
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}

tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions.jvmTarget = "11"
}

dependencies {
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation(platform("software.amazon.awssdk:bom:2.15.6"))
    implementation("com.amazonaws:aws-lambda-java-core:1.2.1")
    implementation("software.amazon.awssdk:auth") {
        exclude("software.amazon.awssdk", "netty-nio-client")
        exclude("software.amazon.awssdk", "apache-client")
    }
    implementation("software.amazon.awssdk:secretsmanager") {
        exclude("software.amazon.awssdk", "netty-nio-client")
        exclude("software.amazon.awssdk", "apache-client")
    }
    implementation("software.amazon.awssdk:url-connection-client")
    implementation("software.amazon.awssdk:xray") {
        exclude("software.amazon.awssdk", "netty-nio-client")
        exclude("software.amazon.awssdk", "apache-client")
    }
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("com.google.code.gson:gson:2.8.6")
    implementation("mysql:mysql-connector-java:8.0.16")
    implementation("org.jetbrains.exposed:exposed-core:0.27.1")
    implementation("org.jetbrains.exposed:exposed-dao:0.27.1")
    implementation("org.jetbrains.exposed:exposed-jdbc:0.27.1")
    implementation("org.jetbrains.exposed:exposed-java-time:0.27.1")
    implementation("org.slf4j:slf4j-api:1.7.30")
    implementation("org.slf4j:slf4j-nop:1.7.30")
    implementation("org.slf4j:slf4j-log4j12:1.7.30")
    implementation("org.liquibase.ext:liquibase-nochangeloglock:1.1")
    implementation("org.liquibase:liquibase-core:3.8.9")
    implementation("org.yaml:snakeyaml:1.24")

    runtimeOnly("com.mattbertolini:liquibase-slf4j:2.0.0")

    testImplementation(platform("org.junit:junit-bom:5.7.0"))
    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.mockito:mockito-core:3.5.13")
    testImplementation("org.mockito:junit-jupiter:2.20.0")
    testImplementation("io.mockk:mockk:1.10.2")
}

task("version") {
    doLast {
        println(version.toString())
    }
}